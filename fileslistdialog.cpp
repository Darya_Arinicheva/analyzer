#include "fileslistdialog.h"
#include "ui_fileslistdialog.h"
#include <QModelIndex>

FilesListDialog::FilesListDialog(QString fileName, QStringList filesList, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FilesListDialog)
{
    ui->setupUi(this);
    this->setModal(true);
    ui->fileNameLabel->setText(fileName);
    ui->filesListWidget->addItems(filesList);
    ui->filesListWidget->setCurrentRow(0);
    selectedItem = filesList.at(0);
    ui->filesListWidget->addItem("None");
}

FilesListDialog::~FilesListDialog()
{
    delete ui;
}

void FilesListDialog::on_okPushButton_clicked()
{
    selectedItem = ui->filesListWidget->currentItem()->text();
    accept();
}

QString FilesListDialog::getSelectedItem()
{
    return selectedItem;
}
