#include "logger.h"

Logger::Logger(QString fileName)
{
    if (fileName == "")
    {
        s = new QTextStream(stdout);
    }
    else
    {
        file = new QFile(fileName);
        file->open(QIODevice::WriteOnly);
        s = new QTextStream(file);
    }
}

Logger::~Logger()
{
    if (file->isOpen())
    {
        file->close();
    }
}

void Logger::log(QString text)
{
    *s << text << "\n";
    s->flush();
}
