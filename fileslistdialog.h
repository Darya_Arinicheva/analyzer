#ifndef FILESLISTDIALOG_H
#define FILESLISTDIALOG_H

#include <QDialog>

namespace Ui {
class FilesListDialog;
}

class FilesListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FilesListDialog(QString fileName, QStringList filesList, QWidget *parent = 0);
    QString getSelectedItem();
    ~FilesListDialog();

private slots:
    void on_okPushButton_clicked();

private:
    Ui::FilesListDialog *ui;
    QString selectedItem;
};

#endif // FILESLISTDIALOG_H
