#include "filemanager.h"
#include <QString>
#include <QStringList>
#include <QFileInfo>
#include <QDir>
#include <QFile>
#include <QPair>
#include <QQueue>
#include <QMultiMap>
#include <QList>
#include <QMap>
#include <QSet>
#include <QDebug>
#include <QTextStream>
#include <QRegExp>
#include <ctime>
#include <unistd.h>
#include <sys/time.h>
#include <chrono>
#include "dependencyparser.h"
#include "filesearcher.h"
#include "cscopehelper.h"
#include "ctagshelper.h"
#include "ctagsinfo.h"
#include "profilegenerator.h"
#include "fileslistdialog.h"

QString getCppFromHeader(QString fileName)
{
    QStringList parts = fileName.split(".");
    QString ans;
    for (int i = 0; i < parts.length() - 1; i++)
    {
        ans = ans + parts[i] + ".";
    }
    ans += "cpp";
//    qDebug() << ans;
    return ans;
}

bool isHeader(QString fileName)
{
    QStringList parts = fileName.split(".");
    return (parts.last() == "h");
}

FileManager::FileManager(QString entryPoint, QStringList standartLibraries,
                         QStringList userLibraries, QString workDirectory, bool isDeepAnalysis) :
    entryPoint(entryPoint), standartLibraries(standartLibraries), userLibraries(userLibraries),
    workDirectory(workDirectory), isDeepAnalysis(isDeepAnalysis), logger("out.log")
{
    try
    {
        QDir dir(workDirectory);
        dir.setNameFilters(QStringList() << "*.*");
        dir.setFilter(QDir::Files);
        foreach(QString dirFile, dir.entryList())
        {
            dir.remove(dirFile);
        }
        if (isDeepAnalysis)
        {
            deepAnalysis();
        }
        else
        {
            QStringList newSources = copyFilesToWorkDirectory(notDeepAnalysis());
            ProFileGenerator::generate(newSources);
        }
    }
    catch (...)
    {

    }
}

QStringList FileManager::notDeepAnalysis()
{
    QStringList answer;
    QMultiMap<QString, QString> standartFilesWithPaths;
    QMultiMap<QString, QString> userFilesWithPaths;
    foreach (QString dir, standartLibraries)
    {
        QDir curDir(dir);
        QList<QPair<QString, QString> > temp = FileSearcher::scanDirectory(curDir);
        QPair<QString, QString> p;
        foreach (p, temp)
        {
            standartFilesWithPaths.insert(p.first, p.second);
        }
    }
    standartFilesMap = standartFilesWithPaths;
    foreach (QString dir, userLibraries)
    {
        QDir curDir(dir);
        QList<QPair<QString, QString> > temp = FileSearcher::scanDirectory(curDir);
        QPair<QString, QString> p;
        foreach (p, temp)
        {
            userFilesWithPaths.insert(p.first, p.second);
        }
    }
    CscopeHelper cscope(userFilesWithPaths.values());
    CtagsHelper ctags(userFilesWithPaths.values());
    QSet<QString> used;
    QFile file(entryPoint);
    QFileInfo fileInfo(file);
    QQueue<QString> q;
    logger.log("file " + fileInfo.absoluteFilePath() + " was included");
    q.append(fileInfo.absoluteFilePath());
    userLibraries.append(fileInfo.absolutePath());
    answer.append(fileInfo.absoluteFilePath());
    while (!q.empty())
    {
        QString curFileName = q.front();
        used.insert(curFileName);
        q.pop_front();
        QFile curFile(curFileName);
        QDir curDir(curFileName.left(curFileName.lastIndexOf("/")));

        QStringList includes = DependencyParser::getIncludeFilesList(curFile);
        foreach(QString include, includes)
        {
            if (include.at(0) == '\"')
            {
                include = include.mid(1, include.length() - 2);
                QString curPath;

                if (include.contains("/"))
                {
                    curDir.cd(include.left(include.lastIndexOf("/")));
                }
                curPath = curDir.absolutePath() + "/" + include.split("/").last();

                include = include.split("/").last();
                QStringList list = userFilesWithPaths.values(include);
                if (list.empty())
                {
                    logger.log("Error! User file" + include + " not found on disk");
                    qDebug() << "Error! File " << include << " not found";
                }
                foreach(QString item, list)
                {
                    if (item == curPath && !used.contains(item))
                    {
                        answer.append(item);
                        q.append(item);
                        logger.log("file " + item + " was included from " + curFileName);
                        bool isFindRealization = false;

                        QStringList sourcesIncluded = cscope.getFilesIncludingThis(item.split("/").last());
                        QString itemDirectory = item.left(item.lastIndexOf("/"));
                        QSet<QString> sourcesIncludedSet = sourcesIncluded.toSet();

                        foreach (QString curClass, ctags.classesInFile.value(item))
                        {
                            foreach (QString memberPath, ctags.membersOfClass.value(curClass))
                            {
                                QString fileDirectory = memberPath.left(memberPath.lastIndexOf("/"));
                                if (userFilesWithPaths.values().contains(memberPath)
                                        && itemDirectory == fileDirectory && sourcesIncludedSet.contains(memberPath))
                                {
                                    isFindRealization = true;
                                    if (!used.contains(memberPath))
                                    {
                                        answer.append(memberPath);
                                        q.append(memberPath);
                                        logger.log("file " + item + " was included from " + memberPath);
                                        //used.append(memberPath);
                                    }
                                }
                            }
                        }

                        QFile source(item);
                        source.open(QIODevice::ReadOnly);
                        QString lines;
                        while (!source.atEnd())
                        {
                            lines.append(source.readLine());
                        }
                        source.close();


                        foreach (QString file, sourcesIncluded)
                        {
                            QString fileDirectory = file.left(file.lastIndexOf("/"));
                            if (itemDirectory == fileDirectory && userFilesWithPaths.values().contains(file))
                            {
                                foreach (QString regexp, ctags.notMembersInFile.value(file))
                                {
                                    if (lines.contains(regexp))
                                    {
                                        isFindRealization = true;
                                        if (!used.contains(file))
                                        {
                                            answer.append(file);
                                            q.append(file);
                                            logger.log("file " + item + " was included from " + file);
                                            //used.append(file);
                                        }
                                    }
                                }
                            }
                            /*if (userFilesWithPaths.values().contains(file) && !used.contains(file))
                            {
                                answer.append(file);
                                q.append(file);
                            }*/
                        }

                        if (!isFindRealization)
                        {
                            logger.log("Error! Realization for file " + item + " not found");
                        }

                        /*if (isHeader(item))
                        {
                            QString source = getCppFromHeader(item);
                            if (userFilesWithPaths.values().contains(source) && !used.contains(source))
                            {
                                answer.append(source);
                                q.append(source);
                            }
                        }*/
                    }
                }
            }
            else
            {
                include = include.mid(1, include.length() - 2);
                QString fullInclude = include;
                include = include.split("/").last();
                QStringList list = userFilesWithPaths.values(include);
                foreach(QString item, list)
                {
                    QString newRegExp(".");
                    if (fullInclude.contains("/"))
                    {
                        newRegExp = "";
                        QStringList parts = fullInclude.split("/");
                        for (int i = 0; i < parts.length() - 1; i++)
                        {
                            newRegExp += (parts[i] + "(/\\w+)*/") ;
                        }
                        newRegExp += parts.last();
                    }
                    QRegExp regExp(newRegExp);

                    bool isFindRealization = false;

                    if ((regExp.indexIn(item) != -1) && !used.contains(item))
                    {


                        answer.append(item);
                        q.append(item);
                        logger.log("file " + item + " was included from " + curFileName);

                        QStringList sourcesIncluded = cscope.getFilesIncludingThis(item.split("/").last());
                        QString itemDirectory = item.left(item.lastIndexOf("/"));
                        QSet<QString> sourcesIncludedSet = sourcesIncluded.toSet();

                        foreach (QString curClass, ctags.classesInFile.value(item))
                        {
                            foreach (QString memberPath, ctags.membersOfClass.value(curClass))
                            {
                                QString fileDirectory = memberPath.left(memberPath.lastIndexOf("/"));
                                if (userFilesWithPaths.values().contains(memberPath)
                                        && itemDirectory == fileDirectory && sourcesIncludedSet.contains(memberPath))
                                {
                                    isFindRealization = true;
                                    if (!used.contains(memberPath))
                                    {
                                        answer.append(memberPath);
                                        q.append(memberPath);
                                        logger.log("file " + item + " was included from " + memberPath);
                                        //used.append(memberPath);
                                    }
                                }
                            }
                        }

                        QFile source(item);
                        source.open(QIODevice::ReadOnly);
                        QString lines;
                        while (!source.atEnd())
                        {
                            lines.append(source.readLine());
                        }
                        source.close();


                        foreach (QString file, sourcesIncluded)
                        {
                            QString fileDirectory = file.left(file.lastIndexOf("/"));
                            if (itemDirectory == fileDirectory && userFilesWithPaths.values().contains(file))
                            {
                                foreach (QString regexp, ctags.notMembersInFile.value(file))
                                {
                                    if (lines.contains(regexp))
                                    {
                                        isFindRealization = true;
                                        if (!used.contains(file))
                                        {
                                            answer.append(file);
                                            q.append(file);
                                            logger.log("file " + item + " was included from " + file);
                                            //used.append(file);
                                        }
                                    }
                                }
                            }
                        }

                        if (!isFindRealization)
                        {
                            logger.log("Error! Realization for file " + item + " not found");
                        }


                        /*
                        answer.append(item);
                        q.append(item);

                        QStringList sourcesIncluded = cscope.getFilesIncludingThis(item.split("/").last());
                        foreach (QString file, sourcesIncluded)
                        {
                            if (userFilesWithPaths.values().contains(file) && !used.contains(file))
                            {
                                answer.append(file);
                                q.append(file);
                            }
                        }
                        */
                        /*if (isHeader(item))
                        {
                            QString source = getCppFromHeader(item);
                            if (userFilesWithPaths.values().contains(source) && !used.contains(source))
                            {
                                answer.append(source);
                                q.append(source);
                            }
                        }*/
                    }
                }
                if (list.empty())
                {
                    list = standartFilesWithPaths.values(include);
                    if (list.empty())
                    {
                        logger.log("Error! Standart file " + include + " not found on disk");
                        qDebug() << "Error! File " << include << " not found";
                    }
                    else
                    {
                        qDebug() << "std file " << include << " found";
                    }
                }
            }
        }
    }
    QSet<QString> ans_set = QSet<QString>::fromList(answer);
    answer = ans_set.values();
    qDebug() << "dependent files: " << answer;
    return answer;
}



void FileManager::deepAnalysis()
{
    auto start = std::chrono::system_clock::now();
    QStringList sourceFiles = notDeepAnalysis();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
    qDebug() << "not deep time: " << elapsed.count();
    CscopeHelper cscope(sourceFiles);
    CtagsHelper ctags(sourceFiles);




    QQueue<QPair<QString, QString> > q; //fooName, fileName
    q.append(qMakePair(QString("main"), entryPoint));
    QMultiMap<QString, CtagsInfo>::iterator it = ctags.map.find("main");
    while (it != ctags.map.end() && it.key() == "main")
    {
        it.value().isUsed = true;
        it++;
    }
    ctags.usedFiles.insert(ctags.map.value("main").file);
    time_t t1 = 0;
    time_t t2 = 0;
    time_t t3 = 0;
    time_t t4 = 0;

    QSet<QString> classesSet = ctags.classes.toSet();


    while (!q.empty())
    {
        qDebug() << "queue size: " << q.length();
        QString curFunction = q.front().first;
        QString curFileName = q.front().second;

        q.pop_front();

        //find function in file
        start = std::chrono::system_clock::now();

        QFile source(curFileName);
        source.open(QIODevice::ReadOnly);
        QString lines;
        while (!source.atEnd())
        {
            lines.append(source.readLine());
        }
        source.close();
        int beg = lines.indexOf(curFunction);
        int id = beg;
        while (lines.at(id) != '{')
        {
            id++;
        }
        int bal = 1;
        id++;
        while (bal > 0)
        {
            if (lines.at(id) == '{')
            {
                bal++;
            }
            if (lines.at(id) == '}')
            {
                bal--;
            }
            id++;
        }
        if (lines.at(id) == ';')
        {
            id++;
        }
        QString wholeFunction = lines.mid(beg, id - beg);

        //find constructors in files
        QSet<QString> findClasses;

        QRegExp regexp("(\\w+)(\\s+)(\\w+)");
        foreach (QString line, wholeFunction.split("\n"))
        {
            int pos = 0;
            line.remove("*");
            int find = regexp.indexIn(line, pos);
            while (find != -1)
            {
                //qDebug() << "match: " << regexp.cap() << "len: " << regexp.cap(1).length() + regexp.cap(2).length();
                pos += (find + regexp.cap(1).length() + regexp.cap(2).length());
                findClasses.insert(regexp.cap(1));
                find = regexp.indexIn(line, pos);
            }
        }
        foreach (QString cur, findClasses)
        {
            if (classesSet.contains(cur))
            {
                it = ctags.map.find(cur);
                while (it != ctags.map.end() && it.key() == cur)
                {
                    if (!it.value().isUsed)
                    {
                        qDebug() << "ctor find: " << it.value().name;
                        it.value().isUsed = true;
                        ctags.usedFiles.insert(it.value().file);
                    }
                    it++;
                }
            }
        }

        /*foreach (QString cur, ctags.classes)
        {
            if (cscope.isDefaultConstructorUsed(curFunction, cur))
            {
                it = ctags.map.find(cur);
                while (it != ctags.map.end() && it.key() == cur)
                {
                    if (!it.value().isUsed)
                    {
                        qDebug() << "ctor find: " << it.value().name;
                        it.value().isUsed = true;
                        ctags.usedFiles.insert(it.value().file);
                    }
                    it++;
                }
            }
        }*/
        elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
        t1 += elapsed.count();



        start = std::chrono::system_clock::now();
        //find methods of classes to mark class as used
        it = ctags.map.find(curFunction);
        while (it != ctags.map.end() && it.key() == curFunction)
        {
            if (it.value().memberOf != "" && it.value().type == "f")
            {
                QString memberOf = it.value().memberOf.split(":")[1];
                memberOf.remove("\n");
                QMultiMap<QString, CtagsInfo>::iterator it2 = ctags.map.find(memberOf);
                while (it2 != ctags.map.end() && it2.key() == memberOf)
                {
                    if (!it2.value().isUsed)
                    {
                        it2.value().isUsed = true;
                        ctags.usedFiles.insert(it2.value().file);
                    }
                    it2++;
                }
            }
            it++;
        }
        elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
        t2 += elapsed.count();


        start = std::chrono::system_clock::now();
        //find declarations of function in other files
        it = ctags.map.find(curFunction);
        while (it != ctags.map.end() && it.key() == curFunction)
        {
            QString signature = it.value().regExp.mid(2, it.value().regExp.length() - 6);
            QStringList files = cscope.getFilesWithDeclaration(curFunction, signature);
            foreach (QString file, files)
            {
                ctags.usedFiles.insert(file);
            }
            it++;
        }
        elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
        t3 += elapsed.count();



        start = std::chrono::system_clock::now();
        //find called functions
        QStringList calledFunctions = cscope.getCalledFunctions(curFunction);
        foreach (QString func, calledFunctions)
        {
            QList<CtagsInfo> info = ctags.map.values(func);
            if (ctags.map.count(func) != 0)
            {
                it = ctags.map.find(func);
                while (it != ctags.map.end() && it.key() == func) {
                    if (!it.value().isUsed)
                    {
                        //ctags.map[func].isUsed = true;
                        it.value().isUsed = true;
                        ctags.usedFiles.insert(it.value().file);
                        q.append(qMakePair(it.value().name, it.value().file));
                    }
                    it++;
                }
            }
        }
        elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
        t4 += elapsed.count();
    }
    qDebug() << "ctor time: " << t1 / 1000.0;
    qDebug() << "mark class used time: " << t2 / 1000.0;
    qDebug() << "find decl time: " << t3 / 1000.0;
    qDebug() << "find called func time: " << t4 / 1000.0;

    QStringList usedSources = ctags.usedFiles.values();
    //usedSources.append(entryPoint);
    qDebug() << "used src: " << usedSources;
    QStringList newSources = copyFilesToWorkDirectory(usedSources);
    foreach (QString file, usedSources)
    {
        foreach (CtagsInfo info, ctags.map.values())
        {
            if (info.file == file && !info.isUsed)
            {
                QFileInfo fileInfo(file);
                if (info.type == "c" || info.type == "s")
                {
                    removeClassFromFile(workDirectory + "/" + fileInfo.fileName(), info);
                }
                if (info.type == "f")
                {
                    if (info.name.startsWith("~"))
                    {
                        it = ctags.map.find(info.name.mid(1));
                        if (it.value().isUsed)
                        {
                            continue;
                        }
                    }
                    removeMethodFromFile(workDirectory + "/" + fileInfo.fileName(), info);
                    QString signature = info.regExp.mid(2, info.regExp.length() - 6);
                    QStringList parts = signature.split(" ");
                    if (parts.length() <= 1)
                    {
                        continue;
                    }
                    QString name = signature.split("::").last();
                    signature = parts[0] + " " + name;
                    QStringList filesWithDeclaration = cscope.getFilesWithDeclaration(info.name, signature);
                    foreach (QString file, filesWithDeclaration)
                    {
                        QString fileName = file.split("/").last();
                        removeDeclarationFromFile(workDirectory + "/" + fileName, signature + ";");
                    }

                }
            }
        }
    }
    fixIncludes(newSources);
    ProFileGenerator::generate(newSources);
}

void FileManager::fixIncludes(QStringList sources)
{
    QSet<QString> fileNames;
    foreach (QString source, sources)
    {
        fileNames.insert(source.split("/").last());
    }
    foreach (QString source, sources)
    {
        QFile src(source);
        src.open(QIODevice::ReadOnly);
        QString lines;
        while (!src.atEnd())
        {
            lines.append(src.readLine());
        }
        src.close();

        QStringList includes = DependencyParser::getIncludeFilesList(src);
        foreach (QString include, includes)
        {
            QString includeFile = include.mid(1, include.length() - 2);
            QString onlyName = includeFile.split("/").last();
            if (include.at(0) != '\"' && !standartFilesMap.values(onlyName).empty())
            {
                continue;
            }
            if (fileNames.contains(onlyName))
            {
                lines.replace(QRegExp("#include\\s*" + include), "#include \"" + onlyName + "\"");
            }
            else
            {
                lines.replace(QRegExp("#include\\s*" + include), "");
            }
        }
        src.open(QIODevice::WriteOnly);
        QTextStream out(&src);
        out << lines;
        src.close();
    }
}

void FileManager::removeClassFromFile(QString file, CtagsInfo info)
{
    QFile source(file);
    source.open(QIODevice::ReadOnly);
    QString lines;
    while (!source.atEnd())
    {
        lines.append(source.readLine());
    }
    source.close();
//    qDebug() << lines;
//    qDebug() << "lines in file: " << lines.indexOf(info.regExp.mid(2, info.regExp.length() - 6));
    int start_id = lines.indexOf(info.regExp.mid(2, info.regExp.length() - 6));
    int id = start_id + info.regExp.length() - 6;
    while (lines.at(id) != '{')
    {
        id++;
    }
    int bal = 1;
    id++;
    while (bal > 0)
    {
        if (lines.at(id) == '{')
        {
            bal++;
        }
        if (lines.at(id) == '}')
        {
            bal--;
        }
        id++;
    }
    if (lines.at(id) == ';')
    {
        id++;
    }
    lines.remove(start_id, id - start_id);
    source.open(QIODevice::WriteOnly);
    QTextStream out(&source);
    out << lines;
    source.close();
}

void FileManager::removeMethodFromFile(QString file, CtagsInfo info)
{
    if (info.name.startsWith("operator "))
    {
        return;
    }
    QFile source(file);
    source.open(QIODevice::ReadOnly);
    QString lines;
    while (!source.atEnd())
    {
        lines.append(source.readLine());
    }
    source.close();
//    qDebug() << lines;
//    qDebug() << "lines in file: " << lines.indexOf(info.regExp.mid(2, info.regExp.length() - 6));
    int start_id = lines.indexOf(info.regExp.mid(2, info.regExp.length() - 6));
    if (start_id == -1)
    {
        return;
    }
    int id = start_id + info.regExp.length() - 6;
    if (info.regExp.mid(2, info.regExp.length() - 6).contains("{"))
    {
        int offset = info.regExp.mid(2, info.regExp.length() - 6).indexOf("{");
        id = start_id + offset;
    }
    while (lines.at(id) != '{')
    {
        id++;
    }
    int bal = 1;
    id++;
    while (bal > 0)
    {
        if (lines.at(id) == '{')
        {
            bal++;
        }
        if (lines.at(id) == '}')
        {
            bal--;
        }
        id++;
    }
    lines.remove(start_id, id - start_id);
    source.open(QIODevice::WriteOnly);
    QTextStream out(&source);
    out << lines;
    source.close();
}

void FileManager::removeDeclarationFromFile(QString file, QString declaration)
{
    QFile source(file);
    source.open(QIODevice::ReadOnly);
    QString lines;
    while (!source.atEnd())
    {
        QString line = source.readLine();
        if (line.contains(declaration))
        {
            continue;
        }
        lines.append(line);
    }
    source.close();
    source.open(QIODevice::WriteOnly);
    QTextStream out(&source);
    out << lines;
    source.close();
}

QStringList FileManager::copyFilesToWorkDirectory(QStringList files)
{
    QStringList ans;
    foreach (QString file, files)
    {
        QFileInfo info(file);
        if (QFile::exists(workDirectory + "/" + info.fileName()))
        {
            QFile::remove(workDirectory + "/" + info.fileName());
        }
        if (!QFile::copy(file, workDirectory + "/" + info.fileName()))
        {
            logger.log("Error! File " + file + " was not copied to work directory");
        }
        //qDebug() << (QFile::copy(file, workDirectory + "/" + info.fileName()) ? "file was copied" : "file was not copied");
        ans.append(workDirectory + "/" + info.fileName());
    }
    qDebug() << "new files: " << ans;
    return ans;
}
