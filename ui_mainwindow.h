/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *standartLibrariesLabel;
    QLabel *userFilesLabel;
    QLabel *workDirectoryLabel;
    QLineEdit *standartLibrariesLineEdit;
    QLineEdit *userFilesLineEdit;
    QLineEdit *workDirectoryLineEdit;
    QPushButton *standartLibrariesPathPushButton;
    QPushButton *userFilesPushButton;
    QPushButton *workDirectoryPushButton;
    QLineEdit *entryPointLineEdit;
    QLabel *entryPointLabel;
    QPushButton *entryPointPushButton;
    QGroupBox *analysisModeGroupBox;
    QCheckBox *analysisModeCheckBox;
    QPushButton *doAnalysisPushButton;
    QMenuBar *menuBar;
    QStatusBar *statusBar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(558, 334);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 541, 152));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        standartLibrariesLabel = new QLabel(gridLayoutWidget);
        standartLibrariesLabel->setObjectName(QString::fromUtf8("standartLibrariesLabel"));

        gridLayout->addWidget(standartLibrariesLabel, 1, 0, 1, 1);

        userFilesLabel = new QLabel(gridLayoutWidget);
        userFilesLabel->setObjectName(QString::fromUtf8("userFilesLabel"));

        gridLayout->addWidget(userFilesLabel, 2, 0, 1, 1);

        workDirectoryLabel = new QLabel(gridLayoutWidget);
        workDirectoryLabel->setObjectName(QString::fromUtf8("workDirectoryLabel"));

        gridLayout->addWidget(workDirectoryLabel, 3, 0, 1, 1);

        standartLibrariesLineEdit = new QLineEdit(gridLayoutWidget);
        standartLibrariesLineEdit->setObjectName(QString::fromUtf8("standartLibrariesLineEdit"));

        gridLayout->addWidget(standartLibrariesLineEdit, 1, 1, 1, 1);

        userFilesLineEdit = new QLineEdit(gridLayoutWidget);
        userFilesLineEdit->setObjectName(QString::fromUtf8("userFilesLineEdit"));

        gridLayout->addWidget(userFilesLineEdit, 2, 1, 1, 1);

        workDirectoryLineEdit = new QLineEdit(gridLayoutWidget);
        workDirectoryLineEdit->setObjectName(QString::fromUtf8("workDirectoryLineEdit"));

        gridLayout->addWidget(workDirectoryLineEdit, 3, 1, 1, 1);

        standartLibrariesPathPushButton = new QPushButton(gridLayoutWidget);
        standartLibrariesPathPushButton->setObjectName(QString::fromUtf8("standartLibrariesPathPushButton"));

        gridLayout->addWidget(standartLibrariesPathPushButton, 1, 2, 1, 1);

        userFilesPushButton = new QPushButton(gridLayoutWidget);
        userFilesPushButton->setObjectName(QString::fromUtf8("userFilesPushButton"));

        gridLayout->addWidget(userFilesPushButton, 2, 2, 1, 1);

        workDirectoryPushButton = new QPushButton(gridLayoutWidget);
        workDirectoryPushButton->setObjectName(QString::fromUtf8("workDirectoryPushButton"));

        gridLayout->addWidget(workDirectoryPushButton, 3, 2, 1, 1);

        entryPointLineEdit = new QLineEdit(gridLayoutWidget);
        entryPointLineEdit->setObjectName(QString::fromUtf8("entryPointLineEdit"));

        gridLayout->addWidget(entryPointLineEdit, 0, 1, 1, 1);

        entryPointLabel = new QLabel(gridLayoutWidget);
        entryPointLabel->setObjectName(QString::fromUtf8("entryPointLabel"));

        gridLayout->addWidget(entryPointLabel, 0, 0, 1, 1);

        entryPointPushButton = new QPushButton(gridLayoutWidget);
        entryPointPushButton->setObjectName(QString::fromUtf8("entryPointPushButton"));

        gridLayout->addWidget(entryPointPushButton, 0, 2, 1, 1);

        analysisModeGroupBox = new QGroupBox(centralWidget);
        analysisModeGroupBox->setObjectName(QString::fromUtf8("analysisModeGroupBox"));
        analysisModeGroupBox->setGeometry(QRect(10, 180, 153, 71));
        analysisModeCheckBox = new QCheckBox(analysisModeGroupBox);
        analysisModeCheckBox->setObjectName(QString::fromUtf8("analysisModeCheckBox"));
        analysisModeCheckBox->setGeometry(QRect(13, 29, 127, 21));
        gridLayoutWidget->raise();
        gridLayoutWidget->raise();
        analysisModeCheckBox->raise();
        doAnalysisPushButton = new QPushButton(centralWidget);
        doAnalysisPushButton->setObjectName(QString::fromUtf8("doAnalysisPushButton"));
        doAnalysisPushButton->setGeometry(QRect(180, 190, 111, 28));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 558, 25));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\320\220\320\275\320\260\320\273\320\270\320\267\320\260\321\202\320\276\321\200 \320\267\320\260\320\262\320\270\321\201\320\270\320\274\320\276\321\201\321\202\320\265\320\271", 0, QApplication::UnicodeUTF8));
        standartLibrariesLabel->setText(QApplication::translate("MainWindow", "\320\241\321\202\320\260\320\275\320\264\320\260\321\200\321\202\320\275\321\213\320\265 \320\261\320\270\320\261\320\273\320\270\320\276\321\202\320\265\320\272\320\270", 0, QApplication::UnicodeUTF8));
        userFilesLabel->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\214\321\201\320\272\320\270\320\265 \321\204\320\260\320\271\320\273\321\213", 0, QApplication::UnicodeUTF8));
        workDirectoryLabel->setText(QApplication::translate("MainWindow", "\320\240\320\260\320\261\320\276\321\207\320\260\321\217 \320\264\320\270\321\200\320\265\320\272\321\202\320\276\321\200\320\270\321\217", 0, QApplication::UnicodeUTF8));
        standartLibrariesPathPushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        userFilesPushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        workDirectoryPushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        entryPointLabel->setText(QApplication::translate("MainWindow", "\320\242\320\276\321\207\320\272\320\260 \320\262\321\205\320\276\320\264\320\260", 0, QApplication::UnicodeUTF8));
        entryPointPushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\320\261\321\200\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
        analysisModeGroupBox->setTitle(QApplication::translate("MainWindow", "\320\240\320\265\320\266\320\270\320\274 \320\260\320\275\320\260\320\273\320\270\320\267\320\260", 0, QApplication::UnicodeUTF8));
        analysisModeCheckBox->setText(QApplication::translate("MainWindow", "\320\223\320\273\321\203\320\261\320\276\320\272\320\270\320\271 \320\260\320\275\320\260\320\273\320\270\320\267", 0, QApplication::UnicodeUTF8));
        doAnalysisPushButton->setText(QApplication::translate("MainWindow", "\320\220\320\275\320\260\320\273\320\270\320\267\320\270\321\200\320\276\320\262\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
