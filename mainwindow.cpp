#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "filemanager.h"
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    /*QString entryPoint = "/home/darkraven/Prog/Analyzer/Analyzer/test/main.cpp";
    QStringList standartLibraries = QStringList() << "/usr/include";
    QStringList userLibraries = QStringList() << "/home/darkraven/Prog/Analyzer/Analyzer/test";
    QString workDirectory = "/home/darkraven/Prog/Analyzer/Analyzer/test/out";
    */

    //5k
    //tree.cpp tree.h
    /*QString entryPoint = "/home/darkraven/Prog/Analyzer/Analyzer/tree_2/in/main_boosting.cpp";
    QStringList standartLibraries = QStringList() << "/usr/include";
    QStringList userLibraries = QStringList() << "/home/darkraven/Prog/Analyzer/Analyzer/tree_2/in";
    QString workDirectory = "/home/darkraven/Prog/Analyzer/Analyzer/tree_2/out";
    */

    //22k
    //newmatap.h newmat.h
    QString entryPoint = "/home/darkraven/Prog/Analyzer/Analyzer/mat/newmat10/example2.cpp";
    QStringList standartLibraries = QStringList() << "/usr/include";
    QStringList userLibraries = QStringList() << "/home/darkraven/Prog/Analyzer/Analyzer/mat/newmat10";
    QString workDirectory = "/home/darkraven/Prog/Analyzer/Analyzer/mat/out";


    ui->entryPointLineEdit->setText(entryPoint);
    ui->standartLibrariesLineEdit->setText(standartLibraries.front());
    ui->userFilesLineEdit->setText(userLibraries.front());
    ui->workDirectoryLineEdit->setText(workDirectory);
    ui->analysisModeCheckBox->setChecked(true);
    ui->doAnalysisPushButton->setFocus();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_entryPointPushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
           tr("All Files (*.*);;C++ Files (*.cpp *.h)"));
    if (fileName != "")
    {
        ui->entryPointLineEdit->setText(fileName);
    }
}

void MainWindow::on_standartLibrariesPathPushButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir != "")
    {
        ui->standartLibrariesLineEdit->setText(dir);
    }
}

void MainWindow::on_userFilesPushButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir != "")
    {
        ui->userFilesLineEdit->setText(dir);
    }
}

void MainWindow::on_workDirectoryPushButton_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir != "")
    {
        ui->workDirectoryLineEdit->setText(dir);
    }
}

void MainWindow::on_doAnalysisPushButton_clicked()
{
    bool isDeep = ui->analysisModeCheckBox->isChecked();
    QString entryPoint = ui->entryPointLineEdit->text();
    QStringList standartLibraries = QStringList() <<  ui->standartLibrariesLineEdit->text(); // "C:/Qt/qtcreator-2.4.1/mingw/lib/gcc/mingw32/4.4.0/include";
    QStringList userLibraries = QStringList() <<  ui->userFilesLineEdit->text(); //"/home/darkraven/Prog/Analyzer/Analyzer/test";
    QString workDirectory = ui->workDirectoryLineEdit->text(); //"/home/darkraven/Prog/Analyzer/Analyzer/test/out";
    FileManager fm(entryPoint, standartLibraries, userLibraries, workDirectory, isDeep);
    QMessageBox message(this);
    message.setText(QString::fromLocal8Bit("Анализ завершен!"));
    message.setWindowTitle(QString::fromLocal8Bit("Анализатор зависимостей"));
    message.exec();
}
