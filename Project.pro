#-------------------------------------------------
#
# Project created by QtCreator 2015-06-03T23:19:26
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXX_FLAGS_WARN_ON += -Wextra
QMAKE_CXX_FLAGS_WARN_ON += -Wall
QMAKE_CXXFLAGS += -std=c++11
CONFIG += c++11

TARGET = Project
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dependencyparser.cpp \
    filesearcher.cpp \
    filemanager.cpp \
    cscopehelper.cpp \
    ctagshelper.cpp \
    ctagsinfo.cpp \
    profilegenerator.cpp \
    fileslistdialog.cpp \
    logger.cpp


HEADERS  += mainwindow.h \
    dependencyparser.h \
    filesearcher.h \
    filemanager.h \
    cscopehelper.h \
    ctagshelper.h \
    ctagsinfo.h \
    profilegenerator.h \
    fileslistdialog.h \
    logger.h

FORMS    += mainwindow.ui \
    fileslistdialog.ui

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE *= -O3
