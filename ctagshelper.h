#ifndef CTAGSHELPER_H
#define CTAGSHELPER_H
#include <QFile>
#include <QProcess>
#include <QTextStream>
#include <QDebug>
#include <QMultiMap>
#include <QSet>

#include "ctagsinfo.h"

class CtagsHelper
{
public:
    CtagsHelper(QStringList sources);
    QStringList getClassesInHeader(QString fileName);
    QMultiMap<QString, CtagsInfo> map;
    QSet<QString> usedFiles;
    QStringList classes;
    QMap<QString, QSet<QString> > classesInFile;
    QMap<QString, QSet<QString> > membersOfClass;
    QMap<QString, QSet<QString> > notMembersInFile;
};

#endif // CTAGSHELPER_H
