#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include <QString>
#include <QStringList>
#include <QMultiMap>
#include <ctagsinfo.h>
#include <logger.h>

class FileManager
{
public:
    FileManager(QString entryPoint, QStringList standartLibraries, QStringList userLibraries, QString workDirectory, bool isDeepAnalysis);
    QStringList notDeepAnalysis();
    void deepAnalysis();
    QStringList copyFilesToWorkDirectory(QStringList files);
    void removeMethodFromFile(QString file, CtagsInfo info);
    void removeClassFromFile(QString file, CtagsInfo info);
    void removeDeclarationFromFile(QString file, QString declaration);
    void fixIncludes(QStringList sources);
private:
    QString entryPoint;
    QStringList standartLibraries;
    QStringList userLibraries;
    QString workDirectory;
    bool isDeepAnalysis;
    QMultiMap<QString, QString> standartFilesMap;
    Logger logger;
};

#endif // FILEMANAGER_H
