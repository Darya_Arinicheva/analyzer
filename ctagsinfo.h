#ifndef CTAGSINFO_H
#define CTAGSINFO_H
#include <QString>

class CtagsInfo
{
public:
    CtagsInfo(QString name, QString file, QString regExp);
    CtagsInfo(const CtagsInfo &other);
    CtagsInfo();
    QString name;
    QString file;
    QString regExp;
    QString type;
    QString memberOf;
    bool isUsed;
};

#endif // CTAGSINFO_H
