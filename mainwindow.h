#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_entryPointPushButton_clicked();

    void on_standartLibrariesPathPushButton_clicked();

    void on_userFilesPushButton_clicked();

    void on_workDirectoryPushButton_clicked();

    void on_doAnalysisPushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
