#include "ctagshelper.h"

CtagsHelper::CtagsHelper(QStringList sources)
{
    QFile file("cscope.files");
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);
    foreach (QString source, sources) {
        out << source << endl;
    }
    file.close();
    QProcess process;
    process.start("ctags -L cscope.files");
    process.waitForFinished(-1);
    QFile tags("tags");
    tags.open(QIODevice::ReadOnly);
    QString line;
    while ((line = tags.readLine()) != "")
    {
        if (line.startsWith("!_TAG_"))
        {
            continue;
        }
        line.remove("\n");
        QStringList parts = line.split("\t");
//        qDebug() << parts;
        CtagsInfo info(parts[0], parts[1], parts[2]);
        if (parts.length() > 3)
        {
            info.type = parts[3];
            if (info.type == "c" || info.type == "s")
            {
                classes.append(parts[0]);
                if (!classesInFile.contains(info.file))
                {
                    QSet<QString> set;
                    set.insert(info.name);
                    classesInFile.insert(info.file, set);
                }
                else
                {
                    classesInFile[info.file].insert(info.name);
                }
            }
        }
        if (parts.length() > 4)
        {
            info.memberOf = parts[4];
            QString memberOf = info.memberOf.split(":").last();
            if (!membersOfClass.contains(memberOf))
            {
                QSet<QString> set;
                set.insert(info.file);
                membersOfClass.insert(memberOf, set);
            }
            else
            {
                membersOfClass[memberOf].insert(info.file);
            }
        }
        else
        {
            QString regexp = info.regExp.mid(2, info.regExp.length() - 6);
            if (!notMembersInFile.contains(info.file))
            {
                QSet<QString> set;
                set.insert(regexp);
                notMembersInFile.insert(info.file, set);
            }
            else
            {
                notMembersInFile[info.file].insert(regexp);
            }
        }
        map.insert(parts[0], info);
    }
    tags.close();
    qDebug() << "classes: " << classes;

}

QStringList CtagsHelper::getClassesInHeader(QString fileName)
{
    QStringList ans;
    QList<CtagsInfo> values = map.values();
    foreach (CtagsInfo info, values)
    {
        if (fileName == info.file && (info.type == "c" || info.type == "s"))
        {
            ans.append(info.name);
        }
    }
    return ans;
}
