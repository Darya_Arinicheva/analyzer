#include "cscopehelper.h"
#include <QRegExp>

CscopeHelper::CscopeHelper(QStringList sources)
{
    QFile file("cscope.files");
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);
    foreach (QString source, sources) {
        out << source << endl;
    }
    file.close();
    QProcess process;
    process.start("cscope -qRbi cscope.files");
    process.waitForFinished(-1);
}

QStringList CscopeHelper::getCalledFunctions(QString function)
{
    QStringList ans;
    QProcess process;
    process.start("cscope -L2 " + function);
    process.waitForFinished(-1);
    QString stdout = process.readAllStandardOutput();
    QStringList functions = stdout.split("\n");
//    qDebug() << "func: " << functions;
    foreach (QString func, functions)
    {
        QStringList names = func.split(" ");
//        qDebug() << names;
        if (names.length() > 1)
        {
            ans.append(names.at(1));
            QString name = names.at(1) + "(";
            for (int i = 3; i < names.length(); i++) {
                if (names.at(i).contains(name))
                {
                    ans.append(names.at(i - 1));
                }
            }
        }
    }
//    qDebug() << "used functions:" << ans;
    return ans;
}

QStringList CscopeHelper::getFilesIncludingThis(QString fileName)
{
    QStringList ans;
    QProcess process;
    process.start("cscope -L8 " + fileName);
    process.waitForFinished(-1);
    QString stdout = process.readAllStandardOutput();
    QStringList functions = stdout.split("\n");
//    qDebug() << "files including dog.h: " << functions;
    foreach (QString func, functions)
    {
        QStringList names = func.split(" ");
//        qDebug() << names;
        if (!names.empty() && names[0] != "")
        {
            ans.append(names[0]);
        }

    }
//    qDebug() << "get files including this:" << ans;
    return ans;
}

QStringList CscopeHelper::getFilesWithDeclaration(QString function, QString signature)
{
    QStringList ans;
    QProcess process;
    process.start("cscope -L0 " + function);
    process.waitForFinished(-1);
    QString stdout = process.readAllStandardOutput();
    QStringList lines = stdout.split("\n");
//    qDebug() << "baba find: " << lines;
    foreach (QString line, lines)
    {
        if (line.contains(signature + ";"))
        {
            ans.append(line.split(" ").first());
        }
    }
//    qDebug() << "baba ans: " << ans;
    return ans;
}

bool CscopeHelper::isDefaultConstructorUsed(QString function, QString className)
{
    QProcess process;
    QRegExp defaultConstuctor(className + "\\s+\\w");
    defaultConstuctor.setMinimal(true);
    process.start("cscope -L0 " + className);
    process.waitForFinished(-1);
    QString stdout = process.readAllStandardOutput();
    QStringList lines = stdout.split("\n");
//    qDebug() << "dog find: " << lines;
    foreach (QString line, lines)
    {
        QStringList parts = line.split(" ");
//        qDebug() << names;
        if (parts.length() > 1 && parts[0] != "" && parts[1] == function)
        {
            if (defaultConstuctor.indexIn(line, 0) != -1)
            {
//                qDebug() << "match! " << line;
                return true;
            }
        }

    }
    return false;
}
