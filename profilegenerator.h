#ifndef PROFILEGENERATOR_H
#define PROFILEGENERATOR_H
#include <QStringList>

class ProFileGenerator
{
public:
    ProFileGenerator();
    static void generate(QStringList sources);
};

#endif // PROFILEGENERATOR_H
