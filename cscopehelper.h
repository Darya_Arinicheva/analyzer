#ifndef CSCOPEHELPER_H
#define CSCOPEHELPER_H
#include <QStringList>
#include <QFile>
#include <QProcess>
#include <QTextStream>
#include <QDebug>

class CscopeHelper
{
public:
    CscopeHelper(QStringList sources);
    QStringList getCalledFunctions(QString function);
    QStringList getFilesIncludingThis(QString fileName);
    QStringList getFilesWithDeclaration(QString function, QString signature);
    bool isDefaultConstructorUsed(QString function, QString className);
};

#endif // CSCOPEHELPER_H
