#include "profilegenerator.h"
#include <QStringList>
#include <QFile>
#include <QTextStream>


ProFileGenerator::ProFileGenerator()
{
}

void ProFileGenerator::generate(QStringList sources)
{
    QStringList parts = sources.first().split("/");
    QString proFilePath;
    for (int i = 0; i < parts.length() - 1; i++)
    {
        proFilePath += (parts[i] + "/");
    }
    proFilePath += "Project.pro";
    QFile pro(proFilePath);
    pro.open(QIODevice::WriteOnly);
    QTextStream out(&pro);
    QString ans = "TEMPLATE = app\n\nSOURCES += ";
    QStringList src;
    QStringList head;
    foreach (QString cur, sources)
    {
        QString name = cur.split("/").last();
        if (name.split(".").last() == "cpp")
        {
            src.append(cur);
        }
        if (name.split(".").last() == "h")
        {
            head.append(cur);
        }
    }
    for (int i = 0; i < src.length(); i++)
    {
        QString name = src[i].split("/").last();
        if (name.split(".").last() == "cpp")
        {
            QString temp = name;
            if (i != 0)
            {
                temp = "\t" + temp;
            }
            if (i == src.length() - 1)
            {
                temp += "\n";
            }
            else
            {
                temp += "\\\n";
            }
            ans += temp;
        }
    }
    ans += "HEADERS += ";
    for (int i = 0; i < head.length(); i++)
    {
        QString name = head[i].split("/").last();
        if (name.split(".").last() == "h")
        {
            QString temp = name;
            if (i != 0)
            {
                temp = "\t" + temp;
            }
            if (i == head.length() - 1)
            {
                temp += "\n";
            }
            else
            {
                temp += "\\\n";
            }
            ans += temp;
        }
    }
    out << ans;
    pro.close();
}
