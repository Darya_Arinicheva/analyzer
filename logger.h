#ifndef LOGGER_H
#define LOGGER_H
#include <QString>
#include <QTextStream>
#include <QFile>

class Logger
{
public:
    Logger(QString fileName = "");
    ~Logger();
    void log(QString text);
private:
    QTextStream* s;
    QFile* file;
};

#endif // LOGGER_H
