#ifndef DEPENDENCYPARSER_H
#define DEPENDENCYPARSER_H
#include <QStringList>
#include <QFile>
class DependencyParser
{
public:
    static QStringList getIncludeFilesList(QFile &source);
};

#endif // DEPENDENCYPARSER_H
