#include "ctagsinfo.h"

CtagsInfo::CtagsInfo(QString name, QString file, QString regExp) : name(name), file(file), regExp(regExp)
{
    isUsed = false;
    type = "";
    memberOf = "";
}

CtagsInfo::CtagsInfo(const CtagsInfo &other) : name(other.name), file(other.file), regExp(other.regExp), type(other.type), memberOf(other.memberOf), isUsed(other.isUsed)
{
}

CtagsInfo::CtagsInfo()
{
    isUsed = false;
    type = "";
    memberOf = "";
}

