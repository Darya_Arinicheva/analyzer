#include "dependencyparser.h"
#include <QTextStream>
#include <QDebug>
#include <QFile>

QStringList DependencyParser::getIncludeFilesList(QFile &source)
{
    source.open(QIODevice::ReadOnly);
    QRegExp localIncludeRegExp("#include.*\".*\"");
    QRegExp globalIncludeRegExp("#include.*<.*>");
    localIncludeRegExp.setMinimal(true);
    globalIncludeRegExp.setMinimal(true);
    QString line;
    QStringList ans;
    while ((line = source.readLine()) != "")
    {
        int pos = 0;
        while ((pos = localIncludeRegExp.indexIn(line, pos)) != -1)
        {
//            qDebug() << "loc iter: " << localIncludeRegExp.capturedTexts();
            int startPos = localIncludeRegExp.capturedTexts().front().indexOf("\"");
            ans.append(localIncludeRegExp.capturedTexts().front().mid(startPos).toLower());
            pos += localIncludeRegExp.matchedLength();
        }
        pos = 0;
        while ((pos = globalIncludeRegExp.indexIn(line, pos)) != -1)
        {
//            qDebug() << "gl iter: " << globalIncludeRegExp.capturedTexts();
            int startPos = globalIncludeRegExp.capturedTexts().front().indexOf("<");
            ans.append(globalIncludeRegExp.capturedTexts().front().mid(startPos).toLower());
            pos += globalIncludeRegExp.matchedLength();
        }
    }
    source.close();
//    qDebug() << ans;
    return ans;
}
