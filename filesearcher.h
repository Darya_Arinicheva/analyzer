#ifndef FILESEARCHER_H
#define FILESEARCHER_H
#include <QString>
#include <QDir>
#include <QString>
#include <QPair>
#include <QList>

class FileSearcher
{
public:
    static QString getPath(QDir &directory, QString fileName);
    static QList<QPair<QString, QString> > scanDirectory(QDir &directory);
private:
    static void scanDirectoryHelper(QDir &directory, QList<QPair<QString, QString> > &ans);

};

#endif // FILESEARCHER_H
