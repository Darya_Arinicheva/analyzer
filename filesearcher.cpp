#include "filesearcher.h"

QString FileSearcher::getPath(QDir &directory, QString fileName)
{
//    foreach (QPair<QString, QString> item, )
    return "d";
}

QList<QPair<QString, QString> > FileSearcher::scanDirectory(QDir &directory)
{
    QList<QPair<QString, QString> > ans;
    scanDirectoryHelper(directory, ans);
    return ans;
}

void FileSearcher::scanDirectoryHelper(QDir &directory, QList<QPair<QString, QString> > &ans)
{
    QFileInfoList list = directory.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (QFileInfo file, list)
    {
        if (file.isDir())
        {
            QDir dir(file.absoluteFilePath());
            scanDirectoryHelper(dir, ans);
        }
        else
        {
            ans.append(QPair<QString, QString> (file.fileName(), file.absoluteFilePath()));
        }
    }
}



